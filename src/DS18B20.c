/***********************************************************
 *
 * @author zx108wl
 * @date  2023年11月5日
 * @details 
 * DS18B20的数据总线需要上拉一个5kΩ的电阻，从而提供高电平
 * 访问DS18B20的流程：初始化——ROM命令——功能命令
 *
 * DS18B20 命令集
 * 一、ROM命令
 *	 1、0XF0：搜索ROM
 *	 2、0X33：读取ROM
 *	 3、0X55：匹配ROM
 *	 4、0XCC：跳过ROM
 *	 5、0XEC：警报搜索
 *二、功能命令
 *	 1、0X44：温度转换
 *	 2、0X4E：写入暂存寄存器
 *	 3、0XBE：读取暂存寄存器
 *	 4、0X48：拷贝暂存寄存器
 *	 5、0XB8：召回EEPROM
 *   6、0XB4: 读取供电模式
************************************************************/
#include <DS18B20.H>
#include<LCD1602.h>
#include <REG52.H>

/*****定义DS18b20的管脚*****/
sbit DQ = P1^4;	
static xdata unsigned char 	romID[8] = {0x00};

#define DATA_Set()  (DQ = 1)
#define DATA_Clr()	(DQ = 0)
#define DATA_Read()	(DQ)

static void 			delay(unsigned int t);
static bit  			Init(void);   //初始化DS18B20
static unsigned char 	ReadOneChar(void);
static void 			WriteOneChar(unsigned char dat);


/****************************************
 * @name	delay
 * @brief	延时子程序
 * @param	t : 延时时间(unsigned int)
 * @return	无
****************************************/
static void delay(unsigned int t)
{
	t*=12;
	while(t--);
}

/****************************************
 * @name	Init
 * @brief	DS18B20初始化程序
 * @param	无
 * @return	无
****************************************/
static bit Init(void)
{
	bit init_flag = 0;
	DATA_Set();      		//DQ复位
	delay(1);    			//稍做延时
	DATA_Clr();      		//单片机将DQ拉低
	delay(45);   			//精确延时，大于480us
	DATA_Set();      		//拉高总线
	delay(1);
	init_flag = DATA_Read();
	delay(45);
	return init_flag;
}

/****************************************
 * @name	ReadOneChar
 * @brief	读一个字节
 * @param	无
 * @return	读取的字节(unsigned char)
****************************************/
static unsigned char ReadOneChar(void)
{
	unsigned char i=0;
	unsigned char dat = 0X00;
	for (i = 8; i > 0; i--)
	{
		DATA_Clr();     // 给脉冲信号
		dat >>= 1;
		DATA_Set();     // 给脉冲信号
		if(DATA_Read())
			dat |= 0x80;
		delay(5);
	}
	DATA_Clr();
	delay(1);
	return(dat);
}

/****************************************
 * @name	WriteOneChar
 * @brief	写一个字节
 * @param	dat: 待写字节(unsigned char)
 * @return	无
****************************************/
static void WriteOneChar(unsigned char dat)
{
	unsigned char i = 0;	
	DATA_Set();      		//DQ复位
	delay(45);    			//稍做延时
	for (i = 8; i > 0; i--)
	{
		DATA_Clr();
		delay(1);
		dat & 0x01 ? DATA_Set() : DATA_Clr();
		delay(6);
		DATA_Set();
		dat >>= 1;
	}
}

/****************************************
 * @name	ReadTemperature
 * @brief	读取温度，返回温度值
 * @param	dat: 待写字节(unsigned char)
 * @return	温度
****************************************/
int ReadTemperature(void)
{
  	unsigned char low, high;
  	long int temp=0x0000;
	Init();					// DS18B20初始化
  	WriteOneChar(0xCC);  	// ROM命令：跳过ROM——只有一个从设备，跳过读序号列号的操作
  	WriteOneChar(0x44);  	// 功能命令：温度转换——启动温度转换
	delay(20);
	Init();					// DS18B20初始化
  	WriteOneChar(0xCC);  	// ROM命令：跳过ROM——只有一个从设备，跳过读序号列号的操作
  	WriteOneChar(0xBE);  	// 功能命令：读取暂存寄存器——读取温度寄存器
	low  = ReadOneChar();   // 读低8位
  	high = ReadOneChar();   // 读高8位
	temp = high;
  	temp = (temp <<= 8) | low;
	if(temp & 0X8000)
	{
		temp &= 0xF800;
		temp = ~temp + 1;
		temp = (temp * 0.0625+0.005) * 100;
		temp = ~(temp - 1);
	}
	else
	{
		temp=(temp * 0.0625+0.005) * 100;
	}
  	return temp;
}

/****************************************
 * @name	ReadROM
 * @brief	读取ROM，返回64位ROM编码
 * @param	void
 * @return	64位ROM编码(unsigned char*)
****************************************/
unsigned char* ReadROM(void)
{
	unsigned char i;
	Init();	
  	WriteOneChar(0x33);  	// 跳过读序号列号的操作
	for(i = 0; i < 8; i++)
	{
		romID[7-i] = ReadOneChar();
	}
  	return romID;
}

/****************************************
 * @name	GetROM
 * @brief	返回64位ROM编码
 * @param	void
 * @return	64位ROM编码(unsigned char*)
****************************************/
unsigned char* GetROM(void)
{
  	return romID;
}

/****************************************
 * @name  SetAramTemperature
 * @brief 	读取阈值温度。
 * @param low  需要修改的最低温度（unsigned char）
 * @param high 需要修改的最高温度（unsigned char）
 * @return none
 ****************************************/
bit SetAramTemperature(unsigned char low, unsigned char high)
{
	if(low >= high){
		 return 0;
	}
	Init();					// DS18B20初始化
  	WriteOneChar(0xCC);  	// ROM命令：跳过ROM——只有一个从设备，跳过读序号列号的操作
  	WriteOneChar(0x4E);  	// ROM命令：写入取暂存寄存器
	WriteOneChar(high);	
	WriteOneChar(low - 1);
	WriteOneChar(0X7F);
	Init();					// DS18B20初始化
  	WriteOneChar(0xCC);  	// ROM命令：跳过ROM——只有一个从设备，跳过读序号列号的操作
	WriteOneChar(0x48);		// 将报警温度存入EEPROM
	return 1;
}

/****************************************
 * @name  ReadAramTemperature
 * @brief 	读取阈值温度。
 * 			使用时，需要传入用于存储对应数据的地址
 * @param low  最低温度存储地址（unsigned char*）
 * @param high 最高温度存储地址（unsigned char*）
 * @return none
 ****************************************/
void ReadAramTemperature(unsigned char* low, unsigned char* high)
{
	unsigned char i = 0;
	Init();					// DS18B20初始化
  	WriteOneChar(0xCC);  	// ROM命令：跳过ROM——只有一个从设备，跳过读序号列号的操作
  	WriteOneChar(0xBE);  	// ROM命令：读取暂存寄存器
	ReadOneChar();
	ReadOneChar();
	*high = ReadOneChar();		//Byte2：温度报警上限
	*low  = ReadOneChar() + 1;	//Byte3：温度报警下限
	Init();
}

/****************************************
 * @name  IsOver
 * @brief 	检测温度是否超过阈值。
 * 			超过阈值，返回1；否则返回0
 * @param void none
 * @return 返回阈值验证结果（bit）
 ****************************************/
bit IsOver(void)
{
	Init();					// DS18B20初始化
  	WriteOneChar(0xEC);  	// ROM命令：读取暂存寄存器
	return !((bit)( ReadOneChar() & 0x01));
}

