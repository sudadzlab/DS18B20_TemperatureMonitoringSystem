/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2023-09-01 22:00:14
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-09-07 14:42:44
 * @FilePath: \DS18B20\LCD1602.h
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
#ifndef __LCD_H
#define __LCD_H


void LCD_Init(void);
bit LCD_SetMaxStrlen(unsigned char len);
void LCD_WriteChar(unsigned char x, unsigned char y, unsigned char ch);
void LCD_ClsLine(unsigned char line);
void LCD_Clear(void);
unsigned char LCD_WriteString(unsigned char x, unsigned char y, unsigned char *str);
unsigned char LCD_WriteNum(unsigned char x, unsigned char y, unsigned char len, unsigned char point, long num);
unsigned char LCD_WriteHex(unsigned char x, unsigned char y, unsigned char len, long num, unsigned char mode);
unsigned char LCD_WriteBinary(unsigned char x, unsigned char y, unsigned char num, unsigned char mode);

#endif