/***********************************************************************************************
 * @file main.c
 * @author zx108wl (1696496757@qq.com)
 * @brief 
 * @version 0.1
 * @date 2023-11-05
 * 
 * @copyright Copyright (c) zx108wl,2023
 * Licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * 
 * Unless otherwise specified, this program is for non-commercial use only.
 * 
 * You are free to:
 * - Share: Copy and redistribute the material
 * - Adapt: Remix, transform, and build upon the material
 * 
 * Under the following terms:
 * - Attribution: You must provide appropriate attribution, provide a link to the license, and 
 * indicate if changes were made. You can do this in any reasonable manner, but not in any way 
 * that suggests the licensor endorses you or your use.
 * - Non-Commercial: You may not use this program for commercial purposes.
 * - No Additional Restrictions: You may not apply legal terms or technological measures that 
 * legally restrict others from doing anything the license permits.
 * 
 * See the full license text at https://creativecommons.org/licenses/by-nc/4.0/
 * 
 * 
 *---------------------------------------------------------------------------------------------*
 *                                       使用手册                                               *
 *---------------------------------------------------------------------------------------------*
 * ——、整体介绍                                                                                 *
 *     本程序包含四个界面：View（温度监视界面）、Setting（温度上下限阈值设置界面）、History（历史温   *
 * 度记录界面）、ROMCode（序列码显示界面）。                                                      *
 *     程序综合运用了蓝桥杯开发板自带的DS18B20（温度传感器）、AT24C02（EEPROM模块）、LCD1602(液晶显  *
 * 示屏)、矩阵键盘、蜂鸣器、继电器等组件，实现了温度显示、温度上下限阈值设置、超出阈值报警、温度阈值掉  *
 * 点保存、历史温度记录掉电保存、读取ROM序列码等功能。                                             *
 *     矩阵键盘使用到了S7、S11、S15、S19、S6、S10共计6个按键，实现界面切换、功能切换、记录温度、修改  *
 * 阈值、保存阈值等功能。                                                                        *
 *                                                                                             *
 * 二、功能介绍                                                                                 *
 *     不同的按键在不同的界面体现不同的功能，仅有部分按键是全局功能，以下根据功能和布局展开详细介绍。  *
 *     1、界面切换                                                                              *
 *     界面切换使用S7按键，也只有S7是全局按键。当按下S7后，界面按照View——Setting——History——ROMCode  *
 * ——View——……循环切换到下一界面。                                                                *
 *     2、View界面                                                                             *
 *        *******************************                                                      *
 *        *  View：						*                                                      *
 *        *  27.00℃					   *                                                      *
 *        *******************************                                                      *
 *     此界面为温度监视界面，会动态显示当前温度，温度采集精度使用0.0625的精度，在显示时，保留两位小数。*
 *     在其界面下只有两个有效按键：                                                              *
 *     S7  —— 界面切换。按下将切换至Setting界面                                                  *
 *     S11 —— 当前温度记录。按下将按照记录顺序存储当前温度值，默认存储值为8个（可通过修改HISTORY_MAX  *
 * 的值调整存储历史温度的个数）。存储值超过个后，将从第一个值开始依次覆盖记录新值。记录的值将储存到     *
 * EEPROM，支持掉电保存                                                                         *
 *     3、Setting界面                                                                           *
 *        *******************************            *******************************           *
 *        *  Setting：      HIGH        *            *  Setting：      HIGH        *            *
 *        *  L:27℃         H:32℃      *            *  L:27℃         H:32℃      *            *
 *        *******************************            *******************************           *
 *     Setting界面可以理解为两个子界面，这两子界面分别设置温度阈值的上限和下限。这修改的阈值直接存储到 *
 * DS18B20内部自带的EEPROM，同时使用DS18B20内部自带的温度报警功能，进行超出阈值判断，最终通过读取     *
 * DS18B0返回的判断结果决定开启警报。                                                            *
 *     此界面的有效按键最多，涵盖了列举出来的所有按键：                                            *
 *     S7  —— 界面切换。按下将切换至History界面                                                  *
 *     S11 —— 切换设置上限与下限。默认为设置上限界面（如左图），按下S11切换到下限设置界面（如右图）， *
 * 再按一下回到左图                                                                             *
 *     S15 —— 修改值加1                                                                        *
 *     S19 —— 修改值减1                                                                        *
 *     S6  —— 取消修改值，返回DS18B20存储的阈值                                                  *
 *     S10 —— 确认修改值，同时修改DS18B20存储的阈值，支持掉电保存                                  *
 *     4、History界面                                                                           *
 *        *******************************                                                      *
 *        *  History：				1   *                                                      *
 *        *  27.00℃					   *                                                      *
 *        *******************************                                                      *
 *     History界面可以理解为八个子界面，这八个子界面分别显示八个历史温度。界面通过右上角的标识进行区  *
 * 分。                                                                                        *
 *     此界面的有效按键使用了3个按键：                                                           *
 *     S7  —— 界面切换。按下将切换至ROMCode界面                                                  *
 *     S15 —— 查看下一记录值，超过最大计数个数后，回到第一个记录值                                 *
 *     S19 —— 查看上一记录值，回到第一个记录之后，下次按下回到最后一个记录值                        *
 *     5、History界面                                                                           *
 *        *******************************                                                      *
 *        *  ROMCode：				    *                                                      *
 *        *  C700000B90510828           *                                                      *
 *        *******************************                                                      *
 *     ROMCode界面只显示当前连接的DS18B20的序列码（DS12B20的序列码最后两位一定是28，否则读取错误     *
 *     此界面只使用了一个有效按键：                                                              *
 *     S7  —— 界面切换。按下将切换至View界面                                                     *
 *     6、温度报警                                                                              *
 *     无论在任何界面，只要DS18B20的温度超过了存储的阈值，整个系统就会报警，直到温度回到正常状态。    *
 *                                                                                            *
 * 三、程序申明                                                                                *
 *     本程序为作者呕心沥血敲出来的，中间遇到了许多稀奇古怪的问题，通过不断调试，才得以解决。同时本代  *
 * 码在DS18B20的使用上远超于网上所给的案例教程，其参考价值远超于网上所有流通的代码。请各位用户在使用  *
 * 过程中，注重功能实现思路，不要一味地照搬硬套。只有通过自己的理解，才能真正学到其中地奥妙。另外，本  *
 * 代码严谨出作者外的人员用于商用，请各位用户合理使用代码，违法必究！                               *
 *                                                                                            *
 **********************************************************************************************/
#include "STC15F2K60S2.H"  //用此头文件直接代替REG52.H，请进行两个头文件对比
#include <DS18B20.H>
#include <LCD1602.h>
#include "stdio.h"
#include <IIC_EEPROM.h>

// 常量宏定义
#define HISTORY_MAX 				8				/*最大历史记录个数*/

// 变量声明
bit 				key_flag 		= 	0;			// 键盘扫描标志位
unsigned char 		key_value 		= 	0xff;		// 键盘读取值
unsigned char 		mode			=	0;			// 界面模式
bit 				setting_mode 	= 	0;			// Setting界面，上下限温度设置选择
xdata unsigned char limit_temp_max	=	30;			// 温度上限阈值
xdata unsigned char limit_temp_min	=	20;			// 温度下限阈值
xdata unsigned int 	history_temp[8]	=	{0x0000};	// 历史温度暂存数组
unsigned char 		history_count	=	0;			// 历史温度当前记录个数
char 				view_count		=	0;			// 历史温度查看位置记录		
bit 				cls				=	1;			// 清屏标志位


// 函数声明
void Timer0					(void);					//Timer0中断服务程序
void Timer0Init				(void);					//Timer0定时器初始化
void read_keyboard			(void);					//读取矩阵键盘键值
void key_action				(void);					//通过判断key_value值，并根据不同的界面执行不同的操作
void SaveTemperature		(void);					//保存所有历史温度到EEPROM里
void UpdateTemperature		(void);					//从EEPROM里读取所有历史温度
void Alarm					(bit flag);				//警报操作
void display				(void);					//LCD界面展示
void Delay100ms				(void);					//延时100ms，为开机动画准备
void PowerOn				(void);					//开机动画显示



/**********************************************************
 * @name Timer0
 * @brief 	Timer0中断服务程序
 * @param void
 * @return none
 **********************************************************/
void Timer0(void) interrupt 1
{
	
	static unsigned char icount = 0;		// 定时辅助计数
	if(icount>=50)
	{
		key_flag = 1;
		icount=0;
	}
	icount++;
}

/**********************************************************
 * @name Timer0Init
 * @brief 	Timer0定时器初始化
 * @param void
 * @return none
 **********************************************************/
void Timer0Init(void)		//1毫秒@12.000MHz
{
	AUXR |= 0x80;		//定时器时钟1T模式
	TMOD &= 0xF0;		//设置定时器模式
	TL0 = 0x20;		//设置定时初值
	TH0 = 0xD1;		//设置定时初值
	TF0 = 0;		//清除TF0标志
	TR0 = 1;		//定时器0开始计时
	ET0=1;
	EA=1;
}

/**********************************************************
 * @name read_keyboard
 * @brief 	读取矩阵键盘键值，并储存读取值到全局变量key_value
 * 			转接板中使用P42和P44代替8051引脚
 *			顺序中的P36和P37引脚
 * @param void
 * @return none
 **********************************************************/
void read_keyboard(void)
{
    static unsigned char hang;
	static unsigned char key_state=0;	
	switch(key_state)
	{
		case 0:
		{
			P3 = 0x0f; P42 = 0; P44 = 0;
			if(P3 != 0x0f) //有按键按下
			key_state=1;	
		}break;
		case 1:
		{
			P3 = 0x0f; P42 = 0; P44 = 0;
			if(P3 != 0x0f) //有按键按下
			{
				if(P30 == 0)hang = 1;
				if(P31 == 0)hang = 2;
				if(P32 == 0)hang = 3;
				if(P33 == 0)hang = 4;//确定行	    
				switch(hang){
					case 1:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=0;key_state=2;}
						else if(P42 == 0) {key_value=1;key_state=2;}
						else if(P35 == 0) {key_value=2;key_state=2;}
						else if(P34 == 0) {key_value=3;key_state=2;}
					}break;
					case 2:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=4;key_state=2;}
						else if(P42 == 0) {key_value=5;key_state=2;}
						else if(P35 == 0) {key_value=6;key_state=2;}
						else if(P34 == 0) {key_value=7;key_state=2;}
					}break;
					case 3:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=8;key_state=2;}
						else if(P42 == 0) {key_value=9;key_state=2;}
						else if(P35 == 0) {key_value=10;key_state=2;}
						else if(P34 == 0) {key_value=11;key_state=2;}
					}break;
					case 4:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=12;key_state=2;}
						else if(P42 == 0) {key_value=13;key_state=2;}
						else if(P35 == 0) {key_value=14;key_state=2;}
						else if(P34 == 0) {key_value=15;key_state=2;}
					}break;
				}	
			}
			else
			{
				key_state=0;	
			}  	   
		}break;
		case 2:     
		{
			P3 = 0x0f; P42 = 0; P44 = 0;
			if(P3 == 0x0f) //按键放开
			key_state=0;	
		}break;
						
    } 
	
}

/**********************************************************
 * @name key_action
 * @brief 	通过判断key_value值，并根据不同的界面执行不同的操作
 * @param void
 * @return none
 **********************************************************/
void key_action(void)
{

	switch(key_value)
	{
	case 0:		//切换界面
		mode++;
		if(mode>=4){
			mode=0;
		}
		cls=1;
		view_count=0;
		setting_mode = 0;
		if(mode == 1){
			ReadAramTemperature(&limit_temp_min, &limit_temp_max);
		}
		break;
	case 1:
		if(mode == 0)
		{
			if(history_count < HISTORY_MAX)
			{
				history_temp[history_count]=ReadTemperature();
				history_count++;
			}else{
				history_count = 0;
				history_temp[history_count]=ReadTemperature();
				history_count++;
			}			
			SaveTemperature();
		}		
		else if(mode == 1)
		{
			setting_mode = !setting_mode;
		}
		break;
	case 2:
		if(mode==1)
		{
			if(setting_mode){
				if(limit_temp_min < limit_temp_max-1){
					limit_temp_min++;
				}
			}else{
				if(limit_temp_max < 60){
					limit_temp_max++;
				}
			}
		}
		else if (mode==2)
		{
			view_count++;
			if(view_count >= HISTORY_MAX) view_count=0;
		}
		break;
	case 3:
		if(mode==1)
		{
			if(setting_mode){	
				if(limit_temp_min > 10){
					limit_temp_min--;
				}
			}else{
				if(limit_temp_min < limit_temp_max - 1){
					limit_temp_max--;
				}
			}
		}
		else if (mode==2)
		{
			view_count--;
			if(view_count<0) view_count = HISTORY_MAX - 1;
		}
		break;
	case 4:
		if(mode == 1)
		{
			ReadAramTemperature(&limit_temp_min, &limit_temp_max);
		}
		break;
	case 5:
		if(mode == 1)
		{
			SetAramTemperature(limit_temp_min, limit_temp_max);
		}
		break;
	}
	
	key_value = 0xff;
}

/**********************************************************
 * @name SaveTemperature
 * @brief 	保存所有历史温度到EEPROM里
 * @param void
 * @return none
 **********************************************************/
void SaveTemperature(void)
{
	unsigned char i;
	EA = 0;
	for(i=0; i < HISTORY_MAX; i++){
		EEPROM_Write16(i*2,history_temp[i]);
	}
	EA = 1;
}

/**********************************************************
 * @name UpdateTemperature
 * @brief 	从EEPROM里读取所有历史温度
 * @param void
 * @return none
 **********************************************************/
void UpdateTemperature(void)
{
	unsigned char i;
	EA = 0;
	for(i=0; i < HISTORY_MAX; i++){
		history_temp[i] = EEPROM_Read16(i*2);
	}
	EA = 1;
}

/**********************************************************
 * @name Alarm
 * @brief 	警报操作（蜂鸣器响，继电器打开）。
 * 			当flag=1是，开启警报；flag=0时，关闭警报
 * @param flag 警报执行标志（bit）
 * @return none
 **********************************************************/
void Alarm(bit flag)
{
	P2=(P2&0X1F)|0XA0;
	P0=(P0&0XAF);
	if(flag)
		P0=P0|0X50;
	P2=P2&0X1F;
}

/**********************************************************
 * @name display
 * @brief 	LCD界面展示，在适当时间显示对应的界面
 * @param void
 * @return none
 **********************************************************/
void display(void)
{
	char len=-1, i=0;
	unsigned int temp = ReadTemperature();
	unsigned char *romID = ReadROM();

	Alarm(IsOver());
	if(cls)
	{
		LCD_Clear();
		cls=0;
	}
	if(mode == 0)
	{
		LCD_WriteString(0,0,"View:");
		len=LCD_WriteNum(0,1,4,2,temp);
		LCD_WriteChar(len,1,0xdf);
		LCD_WriteChar(len+1,1,'C');
	}
	else if(mode == 1)
	{
		if(setting_mode){
			LCD_WriteString(0,0,"Setting:  LOW ");
			LCD_WriteString(0,1,"L:");
			LCD_WriteNum(2,1,2,0,limit_temp_min);
			LCD_WriteChar(4,1,0xdf);
			LCD_WriteChar(5,1,'C');
			LCD_WriteString(10,1,"H:");
			LCD_WriteNum(12,1,2,0,limit_temp_max);
			LCD_WriteChar(14,1,0xdf);
			LCD_WriteChar(15,1,'C');
		}else{
			LCD_WriteString(0,0,"Setting:  HIGH");
			LCD_WriteString(0,1,"L:");
			LCD_WriteNum(2,1,2,0,limit_temp_min);
			LCD_WriteChar(4,1,0xdf);
			LCD_WriteChar(5,1,'C');
			LCD_WriteString(10,1,"H:");
			LCD_WriteNum(12,1,2,0,limit_temp_max);
			LCD_WriteChar(14,1,0xdf);
			LCD_WriteChar(15,1,'C');
		}
	}
	else if(mode == 2)
	{
		LCD_WriteString(0,0,"History:");
		LCD_WriteNum(15,0,1,0,view_count+1);
		len=LCD_WriteNum(0,1,4,2,history_temp[view_count]);
		LCD_WriteChar(len,1,0xdf);
		LCD_WriteChar(len+1,1,'C');
	}
	else if(mode == 3)
	{
		LCD_WriteString(0,0,"ROMCode:");
		for(i = 0; i < 8; i++)
		{
			LCD_WriteHex(2*i, 1, 2, romID[i], 0);
		}
	}
}

/**********************************************************
 * @name Delay100ms
 * @brief 	延时100ms，为开机动画准备
 * @param void
 * @return none
 **********************************************************/
void Delay100ms(void)	//@11.0592MHz
{
	unsigned char data i, j, k;
	i = 5;
	j = 52;
	k = 195;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

/**********************************************************
 * @name PowerOn
 * @brief 	开机动画显示
 * @param void
 * @return none
 **********************************************************/
void PowerOn(void)
{
	unsigned char i = 0, len = 0;
	len = LCD_WriteString(0, 0, "Boot up");
	for(i=0; i<4; i++)
	{
		LCD_WriteString(len, 0, "   ");
		Delay100ms();
		LCD_WriteString(len, 0, ".  ");
		Delay100ms();
		LCD_WriteString(len, 0, ".. ");
		Delay100ms();
		LCD_WriteString(len, 0, "...");
		Delay100ms();
	}
}

//主函数
void main(void)
{
	LCD_Init();   //LCD初始化
	UpdateTemperature();
	Timer0Init();
	PowerOn();
	while(1)
	{
		if(key_flag)
        {
            key_flag = 0;
			read_keyboard();
			if(key_value != 0xFF){
				key_action();
			}
        } 
		display();
	}
}

