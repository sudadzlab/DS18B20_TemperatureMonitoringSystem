#include "reg52.h"
#include "intrins.h"
#include "IIC_EEPROM.h"
#define DELAY_TIME 5

/** 定义I2C总线时钟线和数据线 */
sbit scl = P2^0;
sbit sda = P2^1;


static void i2c_delay(unsigned char i);
static void i2c_start(void);
static void i2c_stop(void);

static void i2c_sendbyte(unsigned char byt);
static unsigned char i2c_waitack(void);
static unsigned char i2c_receivebyte(void);
static void i2c_sendack(unsigned char ackbit);


/**
* @brief I2C总线中一些必要的延时
*
* @param[in] i - 延时时间调整.
* @return none
*/
static void i2c_delay(unsigned char i)
{
    do
    {
        _nop_();_nop_();_nop_();_nop_();_nop_();
        _nop_();_nop_();_nop_();_nop_();_nop_();
        _nop_();_nop_();_nop_();_nop_();_nop_();		
    }
    while(i--);        
}

/**
* @brief 写操作延时，等待eeprom写入
*
* @param void
* @return none
*/
static void Delay4ms(void)	//@11.0592MHz
{
	unsigned char data i, j;

	_nop_();
	i = 44;
	j = 4;
	do
	{
		while (--j);
	} while (--i);
}


/**
* @brief 产生I2C总线启动条件.
*
* @param[in] none
* @param[out] none
* @return none
*/
static void i2c_start(void)
{
    sda = 1;
    scl = 1;
    i2c_delay(DELAY_TIME);
    sda = 0;
    i2c_delay(DELAY_TIME);
    scl = 0;    
}

/**
* @brief 产生I2C总线停止条件
*
* @param[in] none
* @param[out] none.
* @return none
*/
static void i2c_stop(void)
{
    sda = 0;
    scl = 1;
    i2c_delay(DELAY_TIME);
    sda = 1;
    i2c_delay(DELAY_TIME);       
}

/**
* @brief I2C发送一个字节的数据
*
* @param[in] byt - 待发送的字节
* @return none
*/
static void i2c_sendbyte(unsigned char byt)
{
    unsigned char i;
//
	EA = 0;   //关闭中断，避免因为中断而影响总写读写的时序，导致读写失败。
    for(i=0; i<8; i++){
        scl = 0;
        i2c_delay(DELAY_TIME);
        if(byt & 0x80){
            sda = 1;
        }
        else{
            sda = 0;
        }
        i2c_delay(DELAY_TIME);
        scl = 1;
        byt <<= 1;
        i2c_delay(DELAY_TIME);
    }
	EA = 1;
//
    scl = 0;  
}

/**
* @brief 等待应答
*
* @param[in] none
* @param[out] none
* @return none
*/
static unsigned char i2c_waitack(void)
{
	unsigned char ackbit;
	
    scl = 1;
    i2c_delay(DELAY_TIME);
    ackbit = sda; //while(sda);  //wait ack
    scl = 0;
    i2c_delay(DELAY_TIME);
	
	return ackbit;
}

/**
* @brief I2C接收一个字节数据
*
* @param[in] none
* @param[out] da
* @return da - 从I2C总线上接收到得数据
*/
static unsigned char i2c_receivebyte(void)
{
	unsigned char da;
	unsigned char i;
//
	EA = 0;	
	for(i=0;i<8;i++){   
		scl = 1;
		i2c_delay(DELAY_TIME);
		da <<= 1;
		if(sda) 
			da |= 0x01;
		scl = 0;
		i2c_delay(DELAY_TIME);
	}
	EA = 1;
//
	return da;    
}

/**
* @brief 发送应答
*
* @param[in] ackbit - 设定是否发送应答
* @return - none
*/
static void i2c_sendack(unsigned char ackbit)
{
    scl = 0;
    sda = ackbit;  //0：发送应答信号；1：发送非应答信号
    i2c_delay(DELAY_TIME);
    scl = 1;
    i2c_delay(DELAY_TIME);
    scl = 0; 
	sda = 1;
    i2c_delay(DELAY_TIME);
}


/**
 * @name EEPROM_Write
 * @brief EEPROM写入1字节（8位）数据
 * @param address 地址（unsigned char）
 * @param value 数据（unsigned int）
 */
void EEPROM_Write(unsigned char address,unsigned char value)
{
    i2c_start();
    i2c_sendbyte(0xa0);
    i2c_waitack();
    i2c_sendbyte(address);
    i2c_waitack();
    i2c_sendbyte(value);
    i2c_waitack();
    i2c_stop();

    Delay4ms();
}

/**
 * @name EEPROM_Read
 * @brief EEPROM读取1字节（8位）数据
 * @param address 地址（unsigned char）
 * @return 数据（unsigned int）
 */
unsigned char EEPROM_Read(unsigned char address)
{
	unsigned char value;
  
	i2c_start();
	i2c_sendbyte(0xa0);
	i2c_waitack();
	i2c_sendbyte(address);
	i2c_waitack();
	
	i2c_start();
	i2c_sendbyte(0xa1);
	i2c_waitack();
	value = i2c_receivebyte();
	i2c_sendack(1); 
	i2c_stop();
	return value;
}


/**
 * @name EEPROM_Write16
 * @brief EEPROM写入16位数据
 * @param address 地址（unsigned char）
 * @param value 数据（unsigned int）
 */
void EEPROM_Write16(unsigned char address, unsigned int value)
{
    EEPROM_Write(address, (value>>8));
    EEPROM_Write(address+1, value);
}

/**
 * @name EEPROM_Read16
 * @brief EEPROM读取16位数据
 * @param address 地址（unsigned char）
 * @return 数据（unsigned int）
 */
unsigned int EEPROM_Read16(unsigned char address)
{    
	unsigned char high = 0x00, low = 0x00;
    high = EEPROM_Read(address);
    low  = EEPROM_Read(address + 1);
    return (unsigned int)((high<<8) | low);
}
