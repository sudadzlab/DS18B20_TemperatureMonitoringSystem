/*
 * @Author: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @Date: 2023-09-01 22:00:14
 * @LastEditors: error: error: git config user.name & please set dead value or install git && error: git config user.email & please set dead value or install git & please set dead value or install git
 * @LastEditTime: 2023-09-07 12:34:19
 * @FilePath: \DS18B20\DS18B20.h
 * @Description:
 *
 * Copyright (c) 2023 by ${git_name_email}, All Rights Reserved.
 */
#ifndef __DS18B20_H
#define __DS18B20_H


int ReadTemperature(void);          //读取温度
unsigned char* ReadROM(void);       //读取ROM
unsigned char* GetROM(void);        //获取ROM编码的值
bit SetAramTemperature(unsigned char low, unsigned char high);
void ReadAramTemperature(unsigned char* low, unsigned char* high);
bit IsOver();

#endif